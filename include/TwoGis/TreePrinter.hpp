#pragma once
#include <TwoGis/Tree.hpp>

namespace TwoGis {

class TreePrinter {
 public:
  virtual void Print(std::shared_ptr<Tree> tree) const = 0;
};

}  // namespace TwoGis