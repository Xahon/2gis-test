#pragma once
#include <unordered_set>
#include <map>
#include <string>

namespace TwoGis {

class ArgsParser {
  int argc;
  char** argv;
  std::map<std::string, std::string> flagsValues;
  std::unordered_set<std::string> flags;

 public:
  ArgsParser(int argc, char** argv)
      : argc(argc), argv(argv) {
    // -i "aasdadada" -o dadssda

    for (int i = 0; i < argc; ++i) {
      char* arg = argv[i];
      if (arg[0] == '-') {  // flag
        std::string flag(&arg[1]);
        flags.insert(flag);

        if (i + 1 < argc && argv[i + 1][0] != '-') {
          // value is present (not out of bounds and not starting with -)
          char* value = argv[++i];  // get value and skip next arg (already handled)
          flagsValues.insert({flag, value});
        }
      }
    }
  }

  std::string GetFlagValue(const char* flag) const {
    auto it = flagsValues.find(flag);
    if (it == flagsValues.end()) {
      return "";
    }
    return it->second;
  }

  bool IsFlagSet(const char* flag) const {
    return flags.find(flag) != flags.end();
  }
};

}  // namespace TwoGis