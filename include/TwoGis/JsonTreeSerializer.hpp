#pragma once
#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <TwoGis/TreeSerializer.hpp>

namespace TwoGis {

class JsonTreeSerializer : public TreeSerializer {
 public:
  struct MalformedDataException : public std::exception {
    const char* what() const throw() override {
      return "Trying to deserialize malformed of invalid JSON file";
    }
  };

  void Serialize(std::ostream& os, std::shared_ptr<Tree> tree) const override;
  std::shared_ptr<Tree> Deserialize(std::istream& is) const override;
};

}  // namespace TwoGis