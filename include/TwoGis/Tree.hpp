#pragma once
#include <any>
#include <list>
#include <memory>
#include <ostream>
#include <string>

namespace TwoGis {

class Tree {
  std::any object;
  std::list<std::shared_ptr<Tree>> children;

 public:
  explicit Tree(int i);
  explicit Tree(float i);
  explicit Tree(const std::string& str);
  virtual ~Tree() = default;

  void AddChild(std::shared_ptr<Tree> other);
  std::any& GetObject();
  const std::list<std::shared_ptr<Tree>>& GetChildren() const;
  void Print(std::ostream& os) const;
};

}  // namespace TwoGis