#pragma once
#include <TwoGis/Tree.hpp>
#include <iostream>

namespace TwoGis {

class TreeSerializer {
 public:
  virtual void Serialize(std::ostream& os, std::shared_ptr<Tree> tree) const = 0;
  virtual std::shared_ptr<Tree> Deserialize(std::istream& is) const = 0;
};

}  // namespace TwoGis