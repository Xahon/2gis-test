#pragma once
#include <TwoGis/TreePrinter.hpp>

namespace TwoGis {

class SimpleTreePrinter : public TreePrinter {
 public:
  void Print(std::shared_ptr<Tree> tree) const override;
};

}  // namespace TwoGis