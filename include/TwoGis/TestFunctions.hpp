#pragma once
#include <TwoGis/ArgsParser.hpp>
#include <TwoGis/JsonTreeSerializer.hpp>
#include <TwoGis/SimpleTreePrinter.hpp>
#include <TwoGis/Tree.hpp>
#include <fstream>

static std::shared_ptr<TwoGis::Tree> Test_CreateTestTree() {
  using namespace TwoGis;
  auto l_0 = std::make_shared<Tree>("Hello, World!");
  auto l_0_0 = std::make_shared<Tree>(1);
  auto l_0_1 = std::make_shared<Tree>(2);
  auto l_0_1_0 = std::make_shared<Tree>(45);
  auto l_0_1_0_0 = std::make_shared<Tree>("sdfd");
  auto l_0_1_1 = std::make_shared<Tree>(78);
  auto l_0_2 = std::make_shared<Tree>(3.14159265f);

  l_0->AddChild(l_0_0);
  l_0->AddChild(l_0_1);
  l_0_1->AddChild(l_0_1_0);
  l_0_1_0->AddChild(l_0_1_0_0);
  l_0_1->AddChild(l_0_1_1);
  l_0->AddChild(l_0_2);
  return l_0;
}

static void Test_PrintTree() {
  using namespace TwoGis;
  auto tree = Test_CreateTestTree();
  SimpleTreePrinter printer;
  printer.Print(tree);
}

static void Test_ArgsParser(int argc, char** argv) {
  using namespace TwoGis;
  ArgsParser args(argc, argv);
  auto a = args.GetFlagValue("i");
  auto b = args.GetFlagValue("o");
  auto c = args.IsFlagSet("i");
  auto d = args.IsFlagSet("o");
}

static int Test_SerializeTree(const char* filename) {
  using namespace TwoGis;
  auto tree = Test_CreateTestTree();
  std::fstream file(filename, std::fstream::out);
  if (!file) {
    return 1;
  }
  JsonTreeSerializer serializer;
  serializer.Serialize(file, tree);
}

static int Test_DeserializeTree(const char* filename) {
  using namespace TwoGis;
  std::fstream file(filename, std::fstream::in);
  if (!file) {
    return 1;
  }
  JsonTreeSerializer serializer;
  auto tree = serializer.Deserialize(file);
  SimpleTreePrinter printer;
  printer.Print(tree);
}