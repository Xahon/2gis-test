#include <TwoGis/Tree.hpp>

namespace TwoGis {

Tree::Tree(int i)
    : object(i) {
}

Tree::Tree(float i)
    : object(i) {
}

Tree::Tree(const std::string& str)
    : object(str) {
}

void Tree::AddChild(std::shared_ptr<Tree> other) {
  children.push_back(other);
}

std::any& Tree::GetObject() {
  return object;
}

const std::list<std::shared_ptr<Tree>>& Tree::GetChildren() const {
  return children;
}

void Tree::Print(std::ostream& os) const {
  if (const std::string* v = std::any_cast<std::string>(&object)) {
    os << *v;
  } else if (const int* v = std::any_cast<int>(&object)) {
    os << *v;
  } else if (const float* v = std::any_cast<float>(&object)) {
    os << *v;
  }
}

}  // namespace TwoGis