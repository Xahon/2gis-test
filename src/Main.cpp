#include <TwoGis/ArgsParser.hpp>
#include <TwoGis/JsonTreeSerializer.hpp>
#include <TwoGis/SimpleTreePrinter.hpp>
#include <TwoGis/Tree.hpp>
#include <TwoGis/TestFunctions.hpp>
#include <fstream>

int main(int argc, char** argv) {
  using namespace TwoGis;
  ArgsParser args(argc, argv);

  if (!args.IsFlagSet("i")) {
    std::cerr << "Flag -i is required (path to input file)" << std::endl;
    return 1;
  }
  const std::string inputFilename = args.GetFlagValue("i");
  std::fstream inputFile(inputFilename, std::fstream::in);
  if (!inputFile) {
    auto errorMessage = std::strerror(errno);
    std::cerr << "An error occurred while opening input file: " << inputFilename << std::endl;
    std::cerr << errorMessage << std::endl;
    return 2;
  }

  JsonTreeSerializer serializer;
  std::shared_ptr<Tree> tree;

  try {
    tree = serializer.Deserialize(inputFile);
  } catch (JsonTreeSerializer::MalformedDataException e) {
    std::cerr << e.what() << std::endl;
    return 3;
  }

  SimpleTreePrinter printer;
  printer.Print(tree);

  if (args.IsFlagSet("o")) {
    std::string outputFilename = args.GetFlagValue("o");
    std::fstream outputFile(outputFilename, std::fstream::out);
    if (!outputFile) {
      auto errorMessage = std::strerror(errno);
      std::cerr << "An error occurred while creating/opening output file: " << outputFilename << std::endl;
      std::cerr << errorMessage << std::endl;
    } else {
      serializer.Serialize(outputFile, tree);
    }
  }
  
  return 0;
}