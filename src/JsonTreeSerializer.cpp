#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>
#include <TwoGis/JsonTreeSerializer.hpp>
#include <cassert>

namespace TwoGis {

static void SerializeTree(rapidjson::Writer<rapidjson::OStreamWrapper>& writer, std::shared_ptr<Tree> tree) {
  if (tree == nullptr) {
    return;
  }

  writer.StartObject();
  writer.Key("value");

  if (const std::string* v = std::any_cast<std::string>(&tree->GetObject())) {
    writer.String(v->c_str());
  } else if (const int* v = std::any_cast<int>(&tree->GetObject())) {
    writer.Int(*v);
  } else if (const float* v = std::any_cast<float>(&tree->GetObject())) {
    writer.Double(*v);
  }

  auto children = tree->GetChildren();
  if (children.size() > 0) {
    writer.Key("children");
    writer.StartArray();
    for (auto child : children) {
      SerializeTree(writer, child);
    }
    writer.EndArray();
  }

  writer.EndObject();
}

void JsonTreeSerializer::Serialize(std::ostream& os, std::shared_ptr<Tree> tree) const {
  using namespace rapidjson;
  assert(os.good() && "Output stream is not valid");
  assert(tree != nullptr && "Pointer to root tree node cannot be null");

  OStreamWrapper osw(os);
  Writer<OStreamWrapper> w(osw);
  SerializeTree(w, tree);
}

static std::shared_ptr<Tree> DeserializeTree(rapidjson::Value& entry) {
  using namespace rapidjson;

  if (!entry.IsObject() || !entry.HasMember("value")) {
    throw JsonTreeSerializer::MalformedDataException();
  }

  std::shared_ptr<Tree> tree;
  Value& value = entry["value"];

  if (value.IsInt()) {
    tree = std::make_shared<Tree>(value.GetInt());
  } else if (value.IsDouble()) {
    tree = std::make_shared<Tree>(static_cast<float>(value.GetDouble()));
  } else if (value.IsString()) {
    tree = std::make_shared<Tree>(value.GetString());
  } else {
    std::cerr << "Unknown json entry 'value' - not int, double or string" << std::endl;
  }

  if (entry.HasMember("children")) {
    GenericArray<false, rapidjson::Value> children = entry["children"].GetArray();
    for (int i = 0; i < children.Size(); ++i) {
      tree->AddChild(DeserializeTree(children[i]));
    }
  }

  return tree;
}

std::shared_ptr<Tree> JsonTreeSerializer::Deserialize(std::istream& is) const {
  using namespace rapidjson;
  assert(is.good() && "Input stream is not valid");

  Document d;
  IStreamWrapper isw(is);
  d.ParseStream(isw);
  return DeserializeTree(d);
}

}  // namespace TwoGis