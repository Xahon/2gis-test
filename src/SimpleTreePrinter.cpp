﻿#include <TwoGis/SimpleTreePrinter.hpp>
#include <iostream>
#include <queue>

namespace TwoGis {

static void PrintTree(std::ostream& os, std::shared_ptr<Tree> tree, std::string indent, bool isLast) {
  os << indent << "+- ";
  tree->Print(os);
  os << '\n';

  indent += isLast ? "   " : "|  ";

  int i = 0;
  auto children = tree->GetChildren();
  for (auto child : children) {
    PrintTree(os, child, indent, i == children.size() - 1);
    ++i;
  }
}

void SimpleTreePrinter::Print(std::shared_ptr<Tree> tree) const {
  PrintTree(std::cout, tree, "", true);
}

}  // namespace TwoGis