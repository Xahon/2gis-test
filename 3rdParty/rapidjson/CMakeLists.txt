add_library(rapidjson INTERFACE)
target_include_directories(rapidjson INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
add_library(rapidjson::rapidjson ALIAS rapidjson)